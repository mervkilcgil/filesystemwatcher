#ifndef FILESYSTEMWATCHER_H
#define FILESYSTEMWATCHER_H
#include <QtWidgets/QApplication>
#include <QQmlApplicationEngine>
#include <QQuickView>
#include <QtQuick/QQuickView>
#include <QtQuick/QQuickItem>

#include <QGuiApplication>
#include <QObject>
#include <QStringList>
#include <QMap>
#include <QFileSystemWatcher>
#include <QFile>
#include <QDir>
#include <qqml.h>
#include <QtWidgets/QFileDialog>
#include <QQmlExtensionPlugin>
#include <QAccessible>
#include <QQmlContext>
#include <QtWidgets/QDialog>
#include <QtQuickWidgets/QQuickWidget>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonValue>


class FileSystemWatcher : public QObject
{
  Q_OBJECT

  Q_PROPERTY(QString directoryPath READ directoryPathText WRITE setDirectoryPathText NOTIFY directoryPathTextChanged)
  Q_PROPERTY(QString fileNames READ fileNamesText WRITE setFileNamesText NOTIFY fileNamesTextChanged)
  Q_PROPERTY(QString events READ eventsText WRITE setEventsText NOTIFY eventsTextChanged)

  QML_ELEMENT

public:
  FileSystemWatcher(QObject *parent = 0);
  ~FileSystemWatcher();

  void setWidget(QQuickWidget* viewWidget);
  void addPath(const QString &path);

  void setDirectoryPathText(const QString &a);
  QString directoryPathText();

  void setFileNamesText(const QString &a);
  QString fileNamesText();

  void setEventsText(const QString &a);
  QString eventsText();

  bool addLog(QString watchFolder, QString watchChanges);

public slots:
  void browseButtonClicked();
  void startButtonClicked();
  void stopButtonClicked();

  void directoryUpdated(const QString &path);
  void fileUpdated(const QDir& dir);

signals:
  void directoryPathTextChanged();
  void fileNamesTextChanged();
  void eventsTextChanged();
  

private:
  QFileSystemWatcher* _fileMonitor;
  QMap<QString, QStringList> _currentContents;

  QString _selectedFilePath;
  QString _fileNamesText;
  QString _eventsText;

};
#endif // FILESYSTEMWATCHER_H

