#include "FileSystemWatcher.h"
#include <Windows.h>

FileSystemWatcher::FileSystemWatcher(QObject *parent) : QObject(parent)
{
  _fileMonitor = new QFileSystemWatcher();

  connect(_fileMonitor, SIGNAL(fileChanged(QString)), this, SLOT(fileUpdated(QString)));
  connect(_fileMonitor, SIGNAL(directoryChanged(QString)), this, SLOT(directoryUpdated(QString)));
}

//--------------------------------------------------------------

FileSystemWatcher::~FileSystemWatcher()
{
  delete _fileMonitor;
}

//--------------------------------------------------------------

void FileSystemWatcher::addPath(const QString &path)
{
  QFileInfo filePath(path);

  QFileInfo existingFilePath(_selectedFilePath);
  if (existingFilePath.isDir())
  {
    _fileMonitor->removePath(_selectedFilePath);
  }
  if (filePath.isDir())
  {
    const QDir dirw(path);
    this->_currentContents[path] = dirw.entryList(QDir::NoDotAndDotDot | QDir::AllDirs | QDir::Files, QDir::DirsFirst);
    _fileMonitor->addPath(path);
  }
}

//--------------------------------------------------------------

void FileSystemWatcher::directoryUpdated(const QString &path)
{
  setDirectoryPathText(path);

  _eventsText = "Directory updated: " + path + "\n";
  const QDir dir(path);
  fileUpdated(dir);
  setEventsText(_eventsText);
}

//--------------------------------------------------------------

void FileSystemWatcher::fileUpdated(const QDir &dir)
{
  QStringList newEntryList = dir.entryList(QDir::NoDotAndDotDot | QDir::AllDirs | QDir::Files, QDir::DirsFirst);

  QSet<QString> newDirSet = QSet<QString>::fromList(newEntryList);

  QSet<QString> currentDirSet = QSet<QString>::fromList(this->_currentContents[dir.path()]);

  // Files that have been added
  QSet<QString> newFiles = newDirSet - currentDirSet;
  QStringList newFile = newFiles.toList();

  // Files that have been removed
  QSet<QString> deletedFiles = currentDirSet - newDirSet;
  QStringList deleteFile = deletedFiles.toList();

  // Update the current set
  this->_currentContents[dir.path()] = newEntryList;

  if (!newFile.isEmpty() && !deleteFile.isEmpty())
  {
    // File/Dir is renamed

    if (newFile.count() == 1 && deleteFile.count() == 1)
    {
      _eventsText += "File Renamed from " + newFile.first() + " to " + deleteFile.first() + "\n";
    }
  }

  else
  {
    // New File/Dir Added to Dir
    if (!newFile.isEmpty())
    {
      foreach (QString file, newFile)
      {
        _eventsText += "New Files/Dirs added: " + file + "\n";
      }
    }

    // File/Dir is deleted from Dir

    if (!deleteFile.isEmpty())
    {
      foreach (QString file, deleteFile)
      {
        _eventsText += "Files/Dirs deleted: " + file + "\n";
      }
    }
  }
  setEventsText(_eventsText);
}

//--------------------------------------------------------------
bool FileSystemWatcher::addLog(QString watchFolder, QString watchChanges)
{
  QFile file("WatchAppLogs.json");     // json file
  if (!file.open(QIODevice::ReadOnly)) //read json content
  {
    QJsonObject myJsonObj;

    QFile file("WatchAppLogs.json");
    file.open(QIODevice::WriteOnly | QIODevice::Text);
  }

  QJsonDocument jsonOrg = QJsonDocument::fromJson(file.readAll());
  file.close();

  QJsonObject projectDetails = {{"Folder", watchFolder},
                                {"Changes", watchChanges},
                                {"Date and Time", QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss")}};

  QJsonObject logObj = {{"Logs", projectDetails}};

  QJsonArray arrLog = jsonOrg.array();
  arrLog.push_back(logObj);

  QJsonDocument doc(arrLog);

  if (!file.open(QIODevice::WriteOnly)) //write json content to file.
  {
    //cannot open for write ...
    return false;
  }

  file.write(doc.toJson());
  file.close();
}

//--------------------------------------------------------------

void FileSystemWatcher::browseButtonClicked()
{
  QFileDialog dialog(0);
  dialog.setFileMode(QFileDialog::DirectoryOnly);
  dialog.setOption(QFileDialog::ShowDirsOnly);
  dialog.setOption(QFileDialog::DontUseNativeDialog);
  dialog.setAcceptMode(QFileDialog::AcceptSave);
  QString directory = dialog.getExistingDirectory();
  addPath(directory);
  setDirectoryPathText(directory);
}

//--------------------------------------------------------------

void FileSystemWatcher::startButtonClicked()
{
  const QDir dir(_selectedFilePath);
  QStringList dirFiles = dir.entryList(QDir::NoDotAndDotDot | QDir::AllDirs | QDir::Files, QDir::DirsFirst);
  _fileNamesText = "";
  int indx = 1;
  foreach (QString filName, dirFiles)
  {
    _fileNamesText += QString::number(indx) + ". " + filName + "\n";
    indx++;
  }

  setFileNamesText(_fileNamesText);
  setEventsText(_eventsText);
}

//--------------------------------------------------------------

void FileSystemWatcher::stopButtonClicked()
{
  _fileMonitor->removePath(_selectedFilePath);
}

//--------------------------------------------------------------

void FileSystemWatcher::setDirectoryPathText(const QString &a)
{
  if (a != _selectedFilePath)
  {
    _selectedFilePath = a;

    emit directoryPathTextChanged();
  }
}

//--------------------------------------------------------------

QString FileSystemWatcher::directoryPathText()
{
  return _selectedFilePath;
}

//--------------------------------------------------------------

void FileSystemWatcher::setFileNamesText(const QString &a)
{

  _fileNamesText = a;
  emit fileNamesTextChanged();
}

//--------------------------------------------------------------

QString FileSystemWatcher::fileNamesText()
{
  return _fileNamesText;
}

//--------------------------------------------------------------

void FileSystemWatcher::setEventsText(const QString &a)
{
  _eventsText = a;
  emit eventsTextChanged();
  addLog(_selectedFilePath,_eventsText);
}

//--------------------------------------------------------------

QString FileSystemWatcher::eventsText()
{
  return _eventsText;
}
