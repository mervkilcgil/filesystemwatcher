import QtQuick 2.12
import QtQuick.Window 2.12
import Qt3D.Extras 2.15
import QtQuick.Controls.Imagine 2.3
import QtPositioning 5.14
import QtQuick.Controls.Material 2.0
import QtQuick.Controls 2.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("File System Watcher")

    Text {
        id: fileNames
        x: 41
        y: 23
        width: 249
        height: 336
        text: systemWatcher.fileNames
        font.pixelSize: 12
        horizontalAlignment: Text.AlignHCenter
        wrapMode: Text.WordWrap
        minimumPixelSize: 10
    }

    Text {
        id: events
        x: 333
        y: 23
        width: 269
        height: 331
        text: systemWatcher.events
        font.pixelSize: 12
        horizontalAlignment: Text.AlignHCenter
        wrapMode: Text.WordWrap
        minimumPixelSize: 10
    }

    Button {
        id: browse
        x: 418
        y: 365
        width: 100
        height: 25
        text: qsTr("Browse")
        MouseArea {
            anchors.fill: parent
            onClicked: {
                systemWatcher.browseButtonClicked();
            }
        }
    }

    Button {
        id: startMonitoring
        x: 116
        y: 405
        text: qsTr("Start")
        MouseArea {
            anchors.fill: parent
            onClicked: {
                systemWatcher.startButtonClicked();
            }
        }
    }

    Button {
        id: stopMonitoring
        x: 418
        y: 410
        text: qsTr("Stop")
        MouseArea {
            anchors.fill: parent
            onClicked: {
                systemWatcher.stopButtonClicked();
            }
        }
    }

    Text {
        id: directoryPath
        x: 105
        y: 365
        width: 260
        height: 25
        text: systemWatcher.directoryPath
        font.pixelSize: 12
        wrapMode: Text.WordWrap
    }
}
