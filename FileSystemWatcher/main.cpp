#include<QApplication>
#include <QQmlApplicationEngine>

#include "FileSystemWatcher.h"

int main(int argc, char *argv[])
{
  QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

  QApplication app(argc, argv);

  QQmlApplicationEngine engine;
  const QUrl url(QStringLiteral("qrc:/main.qml"));
  QObject::connect(
      &engine, &QQmlApplicationEngine::objectCreated,
      &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
          QCoreApplication::exit(-1);
      },
      Qt::QueuedConnection);
  engine.load(url);

  FileSystemWatcher wathcer{engine.parent()};

  engine.rootContext()->setContextProperty(QStringLiteral("systemWatcher"), &wathcer);



  return app.exec();
}
